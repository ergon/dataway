package com.seejoke.dataway;

import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yangzhongying
 * @date 2020/4/14 11:52
 * @see com.seejoke.dataway.DataWayApplication
 */
@EnableHasor()
@EnableHasorWeb()
@SpringBootApplication(scanBasePackages = {"com.seejoke.dataway"})
public class DataWayApplication {
  public static void main(String[] args) {
    SpringApplication.run(DataWayApplication.class, args);
  }
}
